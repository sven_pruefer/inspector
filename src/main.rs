extern crate quick_xml;
extern crate serde_json;

use quick_xml::de::from_reader;
use std::io;
use std::fs::File;
use std::path::PathBuf;
use structopt::StructOpt;
use inspector::Opts;
use std::io::{Write, BufReader};
use anyhow::Result;

pub mod input;
pub mod output;

fn run_main(input_file: PathBuf, output_file: Option<PathBuf>) -> Result<()> {
    let output_file_writer: Box<dyn Write> = match output_file {
        Some(x) => {
            Box::new(File::create(&x).expect("Output file could not be created")) as Box<dyn Write>
        }
        None => Box::new(io::stdout()) as Box<dyn Write>,
    };
    let file_reader = BufReader::new(File::open(input_file)?);
    let input_report: inspector::input::Report = from_reader(file_reader)?;
    let output_climate_code = inspector::map_inspect_code_to_code_climate(input_report);
    return Ok(serde_json::to_writer_pretty(output_file_writer, &output_climate_code)?);
}

fn main() {
    let opt: Opts = inspector::Opts::from_args();

    std::process::exit(match run_main(opt.input, opt.output) {
        Ok(_) => 0,
        Err(exc) => {
            eprintln!("Couldn't read input file, does it have the right InspectionCode format? Error: {:?}", exc);
            1
        }
    })
}
