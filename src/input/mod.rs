extern crate serde;

use serde::Deserialize;

#[derive(Debug, Deserialize)]
pub struct Report {
    #[serde(rename = "IssueTypes")]
    pub issue_types: IssueTypes,

    #[serde(rename = "Issues")]
    pub issues: Issues,
}

#[derive(Debug, Deserialize)]
pub struct IssueTypes {
    #[serde(rename = "IssueType", default)]
    pub issues: Vec<IssueType>
}

#[derive(Debug, Deserialize)]
pub struct IssueType {
    #[serde(rename = "Id", default)]
    pub id: String,

    #[serde(rename = "Category", default)]
    pub category: String,

    #[serde(rename = "CategoryId", default)]
    pub category_id: String,

    #[serde(rename = "Description", default)]
    pub description: String,

    #[serde(rename = "Severity")]
    pub severity: Severity,

    #[serde(rename = "WikiUrl", default)]
    pub wiki_url: String
}

#[derive(Debug, Deserialize)]
pub struct Issues {
    #[serde(rename = "Project", default)]
    pub projects: Vec<Project>
}

#[derive(Debug, Deserialize)]
pub struct Project {
    #[serde(rename = "Name", default)]
    pub name: String,

    #[serde(rename = "Issue", default)]
    pub issues: Vec<Issue>
}

#[derive(Debug, Deserialize)]
pub struct Issue {
    #[serde(rename = "TypeId", default)]
    pub type_id: String,

    #[serde(rename = "File", default)]
    pub file: String,

    #[serde(rename = "Offset", default)]
    pub offset: String,

    #[serde(rename = "Line", default)]
    pub line: Option<u32>,

    #[serde(rename = "Message", default)]
    pub message: String
}

#[allow(non_camel_case_types)]
#[derive(Debug, Deserialize)]
pub enum Severity {
    ERROR,
    WARNING,
    SUGGESTION,
    HINT
}
