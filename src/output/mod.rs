extern crate serde;

use serde::Serialize;

#[derive(Debug, Serialize)]
pub struct Issue {
    #[serde(rename = "type")]
    pub type_info: String,

    pub check_name: String,

    pub description: String,

    pub content: Option<String>,

    pub categories: Vec<Category>,

    pub location: Location,

    pub severity: Option<Severity>,

    pub fingerprint: Option<String>,
}

#[allow(non_camel_case_types)]
#[derive(Debug, Serialize, Copy, Clone)]
pub enum Category {
    #[serde(rename = "Bug Risk")]
    BugRisk,
    Clarity,
    Compatibility,
    Complexity,
    Duplication,
    Performance,
    Security,
    Style
}

#[derive(Debug, Serialize)]
pub struct Location {
    pub path: String,

    pub lines: Option<Line>,

    pub positions: Option<Positions>,
}

#[allow(non_camel_case_types)]
#[derive(Debug, Serialize, Copy, Clone)]
pub enum Severity {
    info,
    minor,
    major,
    critical,
    blocker
}

#[derive(Debug, Serialize)]
pub struct Line {
    pub begin: u32,

    pub end: u32,
}

#[derive(Debug, Serialize)]
pub struct Positions {
    pub begin: Position,

    pub end: Position,
}

#[derive(Debug, Serialize)]
#[serde(untagged)]
pub enum Position {
    Line { line: u32, column: u32 },
    Offset { offset: u32 },
}