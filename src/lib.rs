use std::path::PathBuf;
use structopt::StructOpt;
use std::collections::HashMap;
use md5::{Md5, Digest};

pub mod input;
pub mod output;

#[derive(StructOpt, Debug)]
#[structopt(name = "inspector")]
pub struct Opts {
    /// Input file
    #[structopt(parse(from_os_str))]
    pub input: PathBuf,

    /// Output file
    #[structopt(parse(from_os_str))]
    pub output: Option<PathBuf>,
}

pub fn map_inspect_code_to_code_climate(input: input::Report) -> Vec<output::Issue> {
    let (issue_category_id_map, issue_categories_map, issue_description_map, issue_severity_map) =
        parse_issue_types(input.issue_types.issues);
    return input
        .issues
        .projects
        .into_iter()
        .flat_map(
            |project| map_project_to_issues(project, &issue_category_id_map, &issue_categories_map, &issue_description_map, &issue_severity_map)
        )
        .collect::<Vec<_>>();
}

fn parse_issue_types(issue_types: Vec<input::IssueType>) -> (HashMap<String, String>, HashMap<String, output::Category>, HashMap<String, String>, HashMap<String, output::Severity>) {
    let mut issue_category_id_map = HashMap::new();
    let mut issue_categories_map = HashMap::new();
    let mut issue_description_map = HashMap::new();
    let mut issue_severity_map = HashMap::new();
    for issue in issue_types {
        issue_category_id_map.insert(issue.id.to_string(), issue.category.to_string());
        issue_categories_map.insert(issue.id.to_string(), match issue.category_id.as_str() {
            "DeclarationRedundancy" => output::Category::Duplication,
            "CodeSmell" => output::Category::Clarity,
            "BestPractice" => output::Category::Style,
            "LanguageUsage" => output::Category::Style,
            "CodeRedundancy" => output::Category::Duplication,
            "XAMLErrors" => output::Category::BugRisk,
            "CodeInfo" => output::Category::Clarity,
            "ConstraintViolation" => output::Category::BugRisk,
            "CSharpErrors" => output::Category::BugRisk,
            "CodeStyleIssues" => output::Category::Style,
            _ => output::Category::BugRisk
        });
        issue_description_map.insert(issue.id.to_string(), issue.description.to_string());
        issue_severity_map.insert(issue.id.to_string(), match issue.severity {
            input::Severity::ERROR => output::Severity::critical,
            input::Severity::WARNING => output::Severity::major,
            input::Severity::SUGGESTION => output::Severity::minor,
            input::Severity::HINT => output::Severity::info
        });
    }
    return (issue_category_id_map, issue_categories_map, issue_description_map, issue_severity_map);
}

fn map_project_to_issues(project: input::Project,
                         issue_category_id_map: &HashMap<String, String>,
                         issue_categories_map: &HashMap<String, output::Category>,
                         issue_description_map: &HashMap<String, String>,
                         issue_severity_map: &HashMap<String, output::Severity>) -> Vec<output::Issue> {
    return project.issues.into_iter().map(|issue| output::Issue {
        type_info: "issue".to_string(),
        check_name: issue_category_id_map.get(&issue.type_id).unwrap().to_string(),
        description: issue_description_map.get(&issue.type_id).unwrap().to_string(),
        content: Some(issue.message.to_string()),
        categories: vec![*issue_categories_map.get(&issue.type_id).unwrap()],
        location: get_location(&issue),
        severity: Some(*issue_severity_map.get(&issue.type_id).unwrap()),
        fingerprint: Some(get_md5(&issue)),
    }).collect::<Vec<_>>();
}

fn get_md5(issue: &input::Issue) -> String {
    return format!("{:x}", Md5::digest(format!("{}-{}-{}", issue.file, issue.offset, issue.type_id).as_bytes()));
}

fn get_location(issue: &input::Issue) -> output::Location {
    let (offset_start, offset_end) = parse_offset(&issue.offset);
    return match issue.line {
        None => {
            output::Location {
                path: issue.file.to_string(),
                lines: None,
                positions: Some(output::Positions { begin: output::Position::Offset { offset: offset_start }, end: output::Position::Offset { offset: offset_end } }),
            }
        }
        Some(line) => output::Location {
            path: issue.file.to_string(),
            lines: None,
            positions: Some(output::Positions { begin: output::Position::Line { line, column: offset_start }, end: output::Position::Line { line, column: offset_end } }),
        }
    };
}

fn parse_offset(offset: &str) -> (u32, u32) {
    let offsets: Vec<&str> = offset.splitn(2, '-').collect();
    return (offsets.first().unwrap().parse::<u32>().unwrap(), offsets.last().unwrap().parse::<u32>().unwrap());
}
